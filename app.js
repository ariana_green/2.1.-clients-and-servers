const http = require('http');
const querystring = require('querystring');
//=============================================================================
// SERVER
const PORT = process.env.PORT || 3000;
const options = {
  hostname: 'netology.tomilomark.ru',
  path: '/api/v1/hash/',
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  }
};

function parse(data, type) {
  switch (type) {
    case 'application/json':
      data = JSON.parse(data);
      break;
    case 'application/x-www-form-urlencoded':
      data = querystring.parse(data);
      break;
    default: data = null;
  }
  return data;
}

function handler(req, res){
  let data = '';
  req.on('data', chunk => data += chunk);
  req.on('end', () => {
    let incomingData = parse(data, req.headers['content-type']);
    if (!incomingData){
      res.write('Incorrect incoming data!');
      res.end();
      return;
    } 

    let answer = {
      firstName: incomingData.firstName,
      lastName: incomingData.lastName
    };
    options.headers.firstname = answer.firstName;
    data = JSON.stringify({ lastName: answer.lastName });

    let APIrequest = http.request(options);
    APIrequest.on('response', (response) => {
      let hash = '';
      response.on('data', function(chunk) {
        hash += chunk;
      });
      response.on('end', () => {
        answer.hash = JSON.parse(hash).hash;
        res.write(JSON.stringify(answer));
        res.end();
      });
    });
    APIrequest.write(data);
    APIrequest.end();
  });
}

const server = http.createServer();
server.on('error', err => console.error(err));
server.on('request', handler);
server.on('listening', () => {
  console.log('Started HTTP on port %d, awaiting requests...', PORT);
});
server.listen(PORT);

//-----------------------------------------------------------------------------
// REQUEST
// full url: http://netology.tomilomark.ru/api/v1/hash
// API:
// method: POST
// headers
//  firstname:string
//  Content-Type:application/json
// body options
//  lastName:string
// success 200
//  hash:string